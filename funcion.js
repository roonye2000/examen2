//Crear una función JavaScript para guardar los datos de clientes
const postEstudent = async (res) => {
    let cedula = document.getElementById("cedula").value;
    let apellido = document.getElementById("apellido").value;
    let nombre = document.getElementById("nombre").value;
    let direccion = document.getElementById("direccion").value;
    let telefono = document.getElementById("telefono").value;
    let email = document.getElementById("email").value;
    if (validar(cedula, apellido, nombre, direccion, telefono, email)) {
      try {
        await pool.query(
          "INSERT INTO  estudiante (cedula,apellido_es,nombre_es,direccion,telefono,email,)VALUES ($1,$2,$3,$4,$5,$6,)",
          [cedula, apellido, nombre, direccion, telefono, email]
        );
      } catch (error) {
        console.log(error.message);
        res.json({ error: error.message });
      }
    }
  };
  
  function validar(cedula, apellido, nombre, direccion, telefono, email) {
      if (cedula!="" && apellido !="" && nombre!="" && direccion!="" && telefono!="" && email){
          return true;
      }
      return false;
  }
  
